import React from "react";
import "./styles.css";
import { Link } from "react-router-dom";

function Nav() {
	return (
		<div id="main-header">
			<Link className="link" to="/">
				Home
			</Link>
			<Link className="link" to="/todo">
				To Do List
			</Link>
		</div>
	);
}

export default Nav;
