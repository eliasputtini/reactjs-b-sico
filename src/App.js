import React from "react";
import { Provider } from "react-redux";

import "./styles.css";

import Route from "./routes";
import TodoList from "./pages/todo";
import store from "./store";

import Main from "./pages/main";

const App = () => (
	<Provider store={store}>
		<Route />
	</Provider>
);

export default App;
