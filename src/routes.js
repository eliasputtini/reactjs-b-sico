import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Main from "./pages/main";
import Product from "./pages/product";
import Nav from "./components/Nav/";

import TodoList from "./pages/todo";

const Routes = () => (
	<BrowserRouter>
		<Nav />
		<Switch>
			<Route exact path="/" component={Main} />
			<Route exact path="/todo" component={TodoList} />
			<Route path="/products/:id" component={Product} />
		</Switch>
	</BrowserRouter>
);
export default Routes;
